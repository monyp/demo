# demo

## Prerequisites

`az` command must be installed and set up with an Azure Service Principal.

`kubectl` must be installed.

## Usage

    terraform init

	terraform validate
	
	terraform plan
	
	terraform apply
	
	kubectl apply -f deployment.yaml --kubeconfig kubeconfig-dev
	
	kubectl apply -f service.yaml --kubeconfig kubeconfig-dev
	
	kubectl apply -f ingress.yaml --kubeconfig kubeconfig-dev

	kubectl apply -f deployment.yaml --kubeconfig kubeconfig-prod
	
	kubectl apply -f service.yaml --kubeconfig kubeconfig-prod
	
	kubectl apply -f ingress.yaml --kubeconfig kubeconfig-prod


## Get External IP Address

	kubectl describe ingress --kubeconfig kubeconfig-dev

	kubectl describe ingress --kubeconfig kubeconfig-prod

