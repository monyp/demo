variable "cluster_name" {
  description = "AKS cluster name"
  default     = "democluster"
}
variable "env_name" {
  description = "AKS cluster environment"
  default     = "dev"
}
variable "instance_type" {
  default = "standard_d2_v2"
}